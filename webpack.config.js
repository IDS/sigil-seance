const path = require('path')

module.exports = {
  entry: './app/app.js',
  mode: 'development',
  output: {
    path: path.resolve(__dirname, './app/dist/'),
    filename: '[name].bundle.js',
  },
}