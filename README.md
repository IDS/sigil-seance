# Sigil Séance Against Space Billionaires

Welcome to *Sigil Séance Against Space Billionaires*, a protection ritual against "New Space" colonizing overlords and an exploration of peer-to-peer online gathering.

*Sigil Séance* is a web-application to collaboratively draw sigils using [p5.js](https://p5js.org/) and [Yjs](https://docs.yjs.dev/).

🔴 Please see the [website](https://sigil-seance.diagram.institute) to try the app and for more information.

## Installation

Clone this repository then run the following form the root

```bash
npm install
npm run dev
```

## Credits
Project by [Lucile Olympe Haute](https://lucilehaute.fr/) and [David Benqué](https://diagram.institute) 

[Basteleur typeface](https://velvetyne.fr/fonts/basteleur/) by Keussel for Velvetyne
