/* eslint-env browser */
import DoUsername from 'do_username'
import * as Y from 'yjs'
import p5 from 'p5'
// @ts-ignore
import { WebsocketProvider } from 'y-websocket'

const ydoc = new Y.Doc()

// room number

var room_nb = document.getElementById("script").getAttribute("room-number");

if (typeof room_nb === "undefined" ) {
   var room_nb = 'sigil-seance';
}
console.log("Room number:" + room_nb);


// const provider = new WebrtcProvider('codemirror6-demo-room', ydoc)
const provider = new WebsocketProvider(
  // `ws${location.protocol.slice(4)}//${location.host}/ws`, // use the local ws server
  'wss://demos.yjs.dev/ws', // alternatively use the public ws server
  room_nb,
  ydoc
)

// Shared object to update with changes in state
const ymap = ydoc.getMap('sigil state')
ymap.set('spell', 'Type your intention');
ymap.set('cypher', 'sigcan');
ymap.set('ouija_pos', 0);

const awareness = provider.awareness

// add custom username
// https://docs.yjs.dev/getting-started/adding-awareness#adding-common-awareness-features-to-our-collaborative-editor

let emojiAvatars = '🐵 🐒 🐶 🐩 🐺 🐱 🐯 🐴 🐎 🐮 🐷 🐗 🐑 🐫 🐘 🐭 🐹 🐰 🐻 🐨 🐼 🐔 🐣 🐥 🐢 🐍 🐲 🐳 🐬 🐟 🐠 🐡 🐙 🐚 🐌 🐛 🐝 🐞 💐 🌸 💮 🌹 🌺 🌻 🌼 🌷 🌱 🌴 🌵 🌾 🌿 🍀 🍁 🍂 🍃 🍄 💫 🌛 ⛄ 🔥 💧 🌊 🎃 👹 👺 👻 👽 👾'.split(/\s+/);

shuffleArray(emojiAvatars);

const setUsername = () => {
  awareness.setLocalStateField('user', {
    name: usernameInput.value,
    avatar: emojiAvatars[0]
  })
}

const usernameInput = document.querySelector('#username')
usernameInput.value = DoUsername.generate(20)
// observe changes on the input element that contains the username
usernameInput.addEventListener('input', setUsername)
setUsername() // intitialise username

// Consent

let consentButton = document.getElementById("consent_btn");

const defaultConsent = () => {
  awareness.setLocalStateField('consent', {
    given: false
  })
  document.querySelector('.consent').style.display = "unset"
}

const giveConsent = () => {
  awareness.setLocalStateField('consent', {
    given: true
  })
  document.querySelector('.consent').style.display = "none"
}

defaultConsent()

consentButton.addEventListener('click', function consent() {
  giveConsent();
});

// User position on grid

const defaultPosition = () => {
  awareness.setLocalStateField('position', {
    col: 0,
    row: 0
  })
}

defaultPosition()


refreshUsers(awareness)
// Render a list of usernames next to the editor whenever new information is available from the awareness instance
awareness.on('change', () => {
  // Map each awareness state to a dom-string
  refreshUsers(awareness)
})

function refreshUsers(awareness) {
  const strings = []
  awareness.getStates().forEach(state => {
    if (state.consent) {
      strings.push(`<div class="consent-${state.consent.given}">${state.user.avatar} ${state.user.name}</div>`)
    }
    document.querySelector('#users').innerHTML = strings.join('')
    var state = awareness.getLocalState()
    var avatar = state.user.avatar
    document.querySelector('#avatar').innerHTML = avatar
  })
}

//let inputText;
let textarea = document.getElementById('sigil_input');
let updateButton = document.getElementById("update_btn");

updateButton.addEventListener('click', function getText() {
  let inputText = textarea.value;
  ymap.set('spell', inputText);
  var spaceless = inputText.replace(/\s/g, "");
  var cypher = removeDuplicate(spaceless);
  ymap.set('cypher', cypher);
  ymap.set('ouija_pos', 0);
  giveConsent(); // automatically consent to own phrase when pressing the button (also shows who submited)
});



// Position Controls

let colButton = document.getElementById("col_btn");
colButton.addEventListener('click', function addCol() {
  var state = awareness.getLocalState()
  var current_pos = state.position
  var new_col = 0
  if (current_pos.col + 1 > 2) {
    new_col = 0
  } else {
    new_col = current_pos.col + 1
  }
  awareness.setLocalStateField('position', {
    col: new_col,
    row: current_pos.row
  })
});

let rowButton = document.getElementById("row_btn");
rowButton.addEventListener('click', function addCol() {
  var state = awareness.getLocalState()
  var current_pos = state.position
  var new_row = 0
  if (current_pos.row + 1 > 2) {
    new_row = 0
  } else {
    new_row = current_pos.row + 1
  }
  awareness.setLocalStateField('position', {
    col: current_pos.col,
    row: new_row
  })
});

// Ouija Control

let ouijaButton = document.getElementById("ouija_btn")
ouijaButton.addEventListener('click', function ouijaUp() {
  var current_pos = ymap.get('ouija_pos')
  var next_pos = current_pos + 1
  var max_pos = ymap.get('cypher').length
  if (next_pos >= max_pos){
    next_pos = 0
  }
  console.log(ymap.get('cypher'), "Letter index " + next_pos + " out of " + max_pos)
  ymap.set('ouija_pos', next_pos)
})

//  P5.js stuff
const P5drawing = ( p ) => {

  const PADDING = 50;
  const ROWS = 3;
  const COLUMNS = 3;
  const CELL_COLOR = '#222';
  const CELL_SIZE = 200;
  const CANVAS_WIDTH = CELL_SIZE * ROWS + (PADDING*2);
  const CANVAS_HEIGHT = CELL_SIZE * COLUMNS + (PADDING*2);
  const CANVAS_COLOR = '#abffdc';
  const MAIN_COLOR = '#c139b6';

  var cell_letters = [
    "ajs", "bkt", "clu",
    "dmv", "enw", "fox",
    "gpy", "hqz", "ir"
  ];
  var cell_positions = [];
  var cell_grid = [];

  // Font
  // https://velvetyne.fr/fonts/basteleur/
  var basteleur;
  p.preload = function() {
    basteleur = p.loadFont("../lib/basteleur-master/fonts/otf/Basteleur-Bold.otf")
  }

  p.setup = function() {
    p.createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
    //background('#000');
    //p.textFont(basteleur)
    var i = 0;
    for (let row=0;row<ROWS;row++) {
      for (let col=0;col<COLUMNS;col++) {
        let left = PADDING+(col*CELL_SIZE);
        let top = PADDING+(row*CELL_SIZE);
        let size = CELL_SIZE;
        cell_positions.push(
          {
              "string": cell_letters[i],
              "x": left + (size / 2),
              "y": top + (size / 2)
          }
        );
        i = ++i;
      }
    }
  }

  function drawPeople () {
    cell_grid = [
      cell_positions.slice(0,3),
      cell_positions.slice(3,6),
      cell_positions.slice(6,10),
    ]
    awareness.getStates().forEach(state => {
      var pos = cell_grid[state.position.row][state.position.col]
      var pad = (CELL_SIZE / 2) - 40
      p.text(state.user.avatar, pos.x + pad, pos.y - pad)
    })
  }

  awareness.on('change', () => {
    p.clear()
    drawPeople()
    drawGrid()
    // Sigil
    drawSigil(ymap.get('cypher'))
    drawOuija()
  })


  // Each time the spell input is updated
  ymap.observe(event => {
    console.log(event.keysChanged)
    p.clear()
    drawGrid()
    drawPeople()
    // Sigil
    drawSigil(ymap.get('cypher'))
    drawOuija()
    textarea.value = ymap.get('spell')
    var whatChanged = event.keysChanged
    if (whatChanged.has('spell')) {
      defaultConsent() // reset everyone's consent if the spell has changed
    }
  });

  function drawGrid() {
    // Grid
    p.strokeWeight(1);
    var i = 0;
    for (let row=0;row<ROWS;row++) {
      for (let col=0;col<COLUMNS;col++) {
        let left = PADDING+(col*CELL_SIZE);
        let top = PADDING+(row*CELL_SIZE);
        let size = CELL_SIZE;
        p.stroke(CANVAS_COLOR);
        p.noFill();
        p.rect(left,top,size,size);
        p.fill(CANVAS_COLOR);
        p.noStroke();
        p.textSize(30);
        p.textFont(basteleur)
        p.text(cell_letters[i], left+10, top+30);
        p.textFont('monospace')
        i = ++i;
      }
    }
  }

  // Drawing the sigil line
  function drawSigil(text) {
    p.noFill();
    p.beginShape();
    p.stroke(MAIN_COLOR);
    p.strokeWeight(5);
    for (let i = 0; i < text.length; i++) {
        var letter = text[i];
        for (let c = 0; c < cell_positions.length; c++) {
            var cell = cell_positions[c]
            if (cell.string.indexOf(letter) > -1 ){
                p.vertex(cell.x, cell.y);
                break;
            }
        }
    }
    p.endShape();
    p.noStroke();
    p.fill('#fff');
    // p.text(ymap.get('cypher'), 10, CANVAS_HEIGHT - 10)
  }

  function drawOuija() {
    var cypher = ymap.get('cypher')
    let letter = cypher.charAt(ymap.get('ouija_pos'))
    var ouija_cell = 0
    for (let c = 0; c < cell_positions.length; c++) {
      var cell = cell_positions[c]
      if (cell.string.indexOf(letter) > -1 ){
          ouija_cell = cell
      }
    }
    console.log('Ouija letter:', letter)
    // Ouija circle on grid
    p.noFill();
    p.stroke(MAIN_COLOR);
    p.strokeWeight(10);
    p.circle(ouija_cell.x,ouija_cell.y,40)
    // Ouija cypher position
    var ouija_cypher = []
    for (let i = 0; i < cypher.length; i++) {
      var current_letter = cypher.charAt(i)
      if (current_letter == letter) {
        ouija_cypher.push(`<span class="currrent">${current_letter}</span>`)
      } else {
        ouija_cypher.push(`<span>${current_letter}</span>`)
      }
    }

    document.querySelector('#ouija_cypher').innerHTML = ouija_cypher.join('')
  }
};

let myp5 = new p5(P5drawing, 'sigil');

// from https://www.geeksforgeeks.org/javascript-program-to-remove-duplicates-from-a-given-string/
function removeDuplicate(string)
{
  string = string.toLowerCase().replace(/[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/g, ''); // Remove punctuation
  var str = string.split("");
  var n = str.length;
  // Used as index in the modified string
  var index = 0;
  // Traverse through all characters
  for (var i = 0; i < n; i++)
  {
    // Check if str[i] is present before it
    var j;
    for (j = 0; j < i; j++)
    {
        if (str[i] == str[j])
        {
            break;
        }
    }
    // If not present, then add it to
    // result.
    if (j == i)
    {
        str[index++] = str[i];
    }
  }
  return str.join("").slice(str, index);
}

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
  }
}
