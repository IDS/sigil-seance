---
title: Sigil Séance Against Space Billionaires
bibliography: /home/david/Library/Zotero_current.bib
csl: /home/david/Library/harvard-cite-them-right.csl
author: 
   - Lucile Olympe Haute, Université de Nîmes
   - David Benqué, Institute of Diagram Studies
date: 8th May 2024, <a href="https://www.ucc.ie/en/future-humanities/radical/conference/">Radical Futures Conference</a>, University College Cork, Ireland
include-before: "<a href='/'>⬅️ Back to homepage</a>"
toc-title: Contents
---

[🖼️ Slide deck](https://ids.codeberg.page/sigil-seance-slides/#/sigil-s%C3%A9ance-against-space-billionaires)

![The passengers of Blue Origin NS-16, the first passenger flight  of Jeff Bezos' New Shepard Rocket. (Jeff Bezos, Mark Bezos, Wally Funk,  Oliver Daemen) - photo: BlueOrigins / Twitter, as found in Marx (2021)](slides.assets/NS-16-crew.png "The passengers of Blue Origin NS-16, the first passenger flight  of Jeff Bezos' New Shepard Rocket. (Jeff Bezos, Mark Bezos, Wally Funk,  Oliver Daemen) - photo: BlueOrigins / Twitter, as found in Marx (2021)")


Our project started in 2021 with the first passenger flight of Jeff Bezos' space-tourism rocket _New Shepard_. This moment acted like a science-fictional vignette, highlighting the figure of the space-billionaire as the arch-villain of our era, a cartoonish character hoarding wealth on an incomprehensible scale, lifting fascistic ideology from sci-fi novels, and leaving a trail of death and destruction behind them.

"Leave the billionaires in space" read the headline of an article by Paris @marx2021a after the New Shepard launch, a sentiment we shared. This paper is about our response to this destructive billionaire figure and the future it promotes. We present *Sigil Séance*, a teleperformance based on chaos magick and sigils, that aims to create a space for thinking critically about the billionaire space race, and to manifest alternatives. As creative practitioners we seek to establish a form of praxis, both reflective and actively engaged in the current manifold crises that are unfolding.


## New Space; the billionaire colonialist/ecocidal industrial complex


![SpaceTech Market Map Valery Komissarov 2016 - [Medium](https://medium.com/@V.K.Komissarov/spacetech-market-map-ca7892ac6590)](slides.assets/1uKj9Nzj7NBYhHi09LCfMAw.jpeg)

While space tourism is attention grabbing, and indeed triggered our own interest in this area, it is only a small part of much bigger shifts currently at play. By opening space exploration to commercial interests in 2015, the United States unleashed a new wave of entrepreneurs and startup companies all looking to profit.

In line with the conference theme, we pay particular attention to the future proposed by space billionaires as a very _radical_ one, in the sense of radicalised and dangerous. We identify at least two vectors of harm in this future: the ecocidal and colonial. In addition, we observe how narratives about outer-space are often deployed to appeal to the imagination but are in fact folded back to reinforce power structures and wealth accumulation on Earth. These lines of inquiry result in the following matrix:


|             | Ecocidal                                                     | Colonial                                                  |
| ----------- | ------------------------------------------------------------ | --------------------------------------------------------- |
| Earth       | Fossil fuels<br>Resource extraction                         | Starlink satellites<br>Land grabs                          |
| Outer-Space | Starman roadster<br>Space junk<br>Inter-planetary contamination | Asteroid mining<br>Moon colonisation<br>Mars colonisation |

:::figcaption
The space-billionaire future
:::

Looking to outer-space first reveals familiar patterns and operations of colonisation. @utrata2023 describes processes of dispossession that 'largely replicate terrestrial histories of colonization'. First delineate a territory, thereby turning it into an asset so that it can be claimed as property.

> “engineering territory,” or the leveraging of new technology to construct virgin territory in ostensibly empty spaces that can then be claimed as territorial property [@utrata2023]

As @smiles2020 points out, the "myths" of American settler colonialism are alive and well in the current discourse around space. Terms such as 'manifest destiny', 'frontier' and underlying concepts such as 'terra nullius' construct Space as at once empty and filled with riches. This language props up projects such as the settling of Mars by Elon @musk2017.  Stunts such as the 2018 launching of a sports car into space—a test payload for Space X's Falcon Heavy rocket—illustrates that for all intents and purposes space-billionaires already consider space their playground. 

![Musk (2017)](slides.assets/elon musk mars colony system.png)

Turning our attention back to Earth orbit reveals a more mundane story, but perhaps even more toxic as it is already partially deployed. Elon Musk's Space X for example has benefited from massive government contracts in the US, effectively replacing NASA with a private entity aimed at capital accumulation. Far from the grand tales of interplanetary conquest, this is about the expansion and control of infrastructure [@au2023]. Amazon for example plans to place datacenters in space for more efficient data streaming via satellites [@liu2019]. Space X has already colonised the sky, wrapping the planet in a tightly coreographed layer of Starlink satellites doing irreparable damage to the night sky.


![Amazon patent US10419106B1 FIG. 1A (inverted by the authors) as cited in @au2023](slides.assets/AWS-from-space.png)

![Starlink Map - [https://www.starlinkmap.org/](https://www.starlinkmap.org/)](slides.assets/starlink map.png)

In summary, the space-billionaire future is a _radical_ ideology of acceleration at a time where all attention should be on planetary limits. At its worse, space-faring is a dark exit strategy, a deep betrayal of life on earth, as it effectively gives up on it and seeks the next planet to pillage. Not only is this promise flawed but entrepreneurs also continually fail to deliver it, as illustrated by the 2017 timeline that shows flights for Mars flights taking place by 2023 (Musk, 2017), the frontier is always 5 years away for Elon Musk.


![Musk (2017)](slides.assets/Musk timeline.png)

@rubenstein2022 demonstrates how the ideological foundations of this space colonialism  are entangled with religion. As the catholic church provided a frame of legality and purpose to European settlers, current colonisation narratives are imbued with a form of tech-messianism.
As we feel the urgency to protect ourselves and the planet from this deadly  future, pushing back on a spiritual level therefore seems like a relevant terrain. 


## A Sigil Séance


Perhaps turning to magic or the occult is also one of the last few options to reclaim a sense of agency as so little else seems available to counter the current direction of travel.


![Osman Spare (1913)](slides.assets/sigil-1.jpeg)


Chaos Magick is a modern tradition of magic characterised by the use of paradigm shifting, free cosmogony and belief as a tool to achieve effects. Famous figures include turn of the 20th century occultist and philosopher Alister Crowley and one of his key sources of inspiration, artist Austin Osman Spare. The latter revived sigils as 'the art of believing' in a 1913 publication, [@osmanspare1913, p.86] a way to mobilise one's unconscious power towards the manifestation of a plan (p.100). 



![Sigils gathered by Jackson (2013), redrawn with additions from the authors](slides.assets/sigil-2.jpeg)

Ciphers are a much older magical tradition, according to Mark B. @jackson2013, they organise letters spatially to allow for the encryption of sentences or words.
We chose to work with a 3 by 3 square cipher grid, somewhere between a magical square[^magicsquare] and the phone keypad cipher dear to elderly millennials like ourselves.

[^magicsquare]: Magical squares were discoverd in India around 650 BC and travelled through arabic countries before arriving in Europe in the 15th century through figures like Agrippa [@jackson2013].


![Bind Billionaires who fly to the upper sky from coming back to Earth - Sigil Generator v1 - [Sigil Generator v1](https://codeberg.org/IDS/Sigil_Generator)](slides.assets/full_sigil.svg)


We started experimenting with sigils back in 2021 with the first generator to encode the phrase: "Bind billionaires who fly to the upper sky from coming back to Earth". 


<figure>
   <img alt="inaugural session screenshot" src="slides.assets/sigil seance inaugural session screenshot.png">
   <video stretch controls autoplay loop>
   <source src="slides.assets/Sigil-séance-04-29-2024(3).mp4" type="video/mp4">
   </video>
<figcaption>Sigil Séance inaugural session - screenshot and video</figcaption>
</figure>


With the Sigil Séance we are adding a collaborative element to this sigil making, allowing for a group of people to gather on a web page to generate and launch sigils together. The application is built with Yjs, a library normally used for collaborative text editing  [@nicolaescu2016; @jahns2014].

As they connect, each participant is given a user-name and an emoji avatar that appear in the presence list on the page and on the cipher grid. Anyone can edit or replace the phrase in the main text box and click a button to draw the sigil and sync it to all other participants. 

### Sigil Launch procedure

Ritual participants are all connected through an audio call. One participant types a phrase to launch and invites edits or additions from the others. 

The software converts each phrase to a sigil by 1) condensing all letters and removing duplicates such that each letter cannot appear again after its first position, and 2) tracing the resulting sequence of letters on the cypher grid. 

All participants must express consent before proceeding to the sigil launch. Consent is reset each time the sigil is changed to ensure that the collective energy cannot be subverted at the last minute.

Participants send and receive letters according to their position on the grid. This process repeats until the whole letter sequence is completed, at which point the sigil is considered launched and one participant clears the grid by sending an empty update.

This launch procedure is repeated a number of times during the séance ritual which we designed as a teleperformance


### Ritual script for a teleperformance (~1 hour)

```
1) Hosts and participants introduce each other
2) Participants are invited to add entities to the formation
   (e.g. Karl Marx, Laika, Vladimir Komarov). 
   Each entity is summoned by launching their name
3) Hosts declare the formation complete
4) Payload: participants are invited to share 
   anti-space-billionaire phrases for 
   collective launching
5) Summoned entities are released by launching their reversed name
6) Hosts declare the formation open
7) Discussion and de-briefing 
```

Ritual is, according to Richard @schechner2002, one of foundational element of performance. In fact performance can be defined simply as ritualised gestures and sounds. Performance practitioners have shown conflicting relationships to technology, and the mediation it operates between performers, participants, and audience. Artists like Allan @kaprow1963 and Tino Sehgal [@gravano2013] consider documentation through photography to hinder the experience of performance, sometimes banning it entirely. On the other hand, Gina @pane2012 considered that looking at photographs from a performance is like attending it in person.

We follow the late legacy of the Cyborg Manifesto [@haraway1991] and embrace its impure ontology that affects the subject and, in our ritual, the performing subjects. Therefore, we characterise our project as a *teleperformance* [@haute2010] with an implicit use of technology. The traditional ouija board or séance arrangement is replaced by a web page within which we collectively charge and launch the sigil of a spell.


## Magick & teleperformance as counter-attack formations

In the field of political activism, collective action is a strategy adopted to resist control mechanisms based on individualisation. 
Our work is also an opportunity to revisit past examples of groups and movements mobilising magic as a means of political action.

![Jusatsu Kito Sodan (photo: Mitsutoshi Hanaga Estate)](slides.assets/Jusatsu Kito Sodan.png)

In 1970s Japan, a group of monks called Jusatsu Kito Sodan were part of the vanguard of the country's environmental movement. Their name translates to 'Killing Curse Prayer Monk Group' and they were documented by photographer Mitsutoshi Hanaga as they traveled to sites of industrial pollution to curse the factory owners responsible through the repeating of mantras and other ritual practices [@hopfner2020].


![W.I.T.C.H. Portland Instagram](slides.assets/13-WITCH-PORTLAND-2.jpg)

![](slides.assets/00-WITCH-GIF.gif)

The Women’s International Terrorist Conspiracy from Hell (W.I.T.C.H.) was a feminist group active in the United States during the 1960s women’s liberation movement. Their first action consisted in a march down Wall Street, to hex New York’s financial district. 
The anonymous collective re-formed in the wake of Donald Trump’s 2016 election, first in Portland and then in many other American cities. The 2016 WITCH explicitly reference to their 1960s predecessors, quoting them in an Instagram post.

![Witches against Trump: #bindtrump & #magicalresistance](slides.assets/bindtrump3.gif)
Following the election of Trump in 2016, the hashtag #BindTrump popped up on Instagram and Twitter to organise the cyber-magic resistance. At each full moon, a ritual rendezvous was performed individually and remotely becoming collective through the hashtags like #magicresistance. This revival of W.I.T.C.H. in the social network era also mobilised a backlash from Trump supporters who gathered through the hashtag #PrayerResistance.

![The Hologram by Cassie Thornton - [https://thehologram.xyz](https://thehologram.xyz)](slides.assets/hologram.png)


Finally, although not technically magical, we'd like to mention [_The Hologram_](https://thehologram.xyz) by Cassie @thornton2020 and especially the notion of "peer-to-peer care" as a collective response to the catastrophic failures of healthcare systems. Here participants assemble in a formation of 3 around the 1 individual to care for, reclaiming some of the care and attention that is denied to them. 


### Taking dark magic seriously

While this project started innocently with a sigil generator, coordinating the ritual activation of sigils by multiple people through a collaborative application forces us to take the potential consequences more seriously.

The Jusatsu Kito Sodan monks mentioned earlier deployed _abhicara_, or black magic, which would be frowned upon by many buddhist who adhere to strict non-violence. According to the group however the amount of deadly damage unleashed by industrial pollution were of such a magnitude that a kill curse was warranted.

> Leaving aside the human toll, by wiping out a fathomless number of no less important organisms, those responsible for industrial pollution threatened <mark>the entire cosmic order</mark>. A curse was therefore the only proportionate response [@hopfner2020]

We also consider that 'the entire cosmic order' is currently under enough threat to warrant serious curses as counter-measures. However, we are also mindful that our application is aimed at inviting others, and we want to provide ways for them to consider their own level of proportionate response.

We designed the consent mechanism (explained above) as a way to encode these intentions in the application. We also added the following disclaimer to our invitation text and website to invite participants to reflect on their position on the séance goals:

> **Disclamer**: A fully committed belief in the power of chaos magick is not required to attend. However we do ask all attendants to consider the potential consequences of this ritual. Taking sigil magick seriously, this ritual may cause a space-billionaire to come to harm or death. Futhermore, in magic, everything that is asked for is considered to have a cost or a return. Please do not attend if you are not comfortable with these potential consequences (and that would be OK)



## Conclusion & Future Work

![Future goal: peer-to-peer séance infrastructure using WebRTC](slides.assets/webrtc-graphviz-circle-peers-comp.svg)

This paper is the first we share about this project so it is a starting point for us. We have identified an area for a kind of praxis between analysing the radicalised future of space-billionaires and providing a space to express our resistance to it in cooperation with others.

We led a successful inaugural session with 4 participants that all had positive feedback and have committed to attending future sessions. With this budding community of practice, the next step will be to give the séances a recurring temporality, perhaps aligned with that of commercial space launches. 

There are a number of technical developments that would be interesting, such as opening up more "rooms" or pages for people to have sigil rituals without our facilitation. We are interested in infrastructural politics and the poetics of peer-to-peer networks so we will also experiment with protocols such as WebRTC where participant devices would be connected directly (after being introduced by a signaling server).

Finally on a more theoretical level, we want to engage more with alternative visions of space and the cosmos. In particular we want to seek out indigenous visions that have been resisting colonial and ecocidal futures for hundreds of years[^further-reading]. In the words of Mary-Jane Rubenstein, we want to seek out better mythologies.

> ...if we want to get right with space, we’re going to have to get right with religion. To expose the values of contemporary techno-science as the product of bad mythologies and seek out better ones. [@rubenstein2022]

Many thanks for reading. The [Sigil Séance application](https://sigil-seance.diagram.institute) is available for testing. You can email feedback or register your interest for future séance sessions at the following address: [sigil-seance@diagram.institute](mailto:sigil-seance@diagram.institute)

[^further-reading]: Further reading includes:  
- [Indigenous Studies Working Group Statement.](https://doi.org/10.17953/aicrj.45.1.atalay_etal) *American Indian Culture and Research Journal* 45(1)   
- [Ethical Exploration and the Role of Planetary Protection in Disrupting Colonial Practices.](https://doi.org/10.3847/25c2cfeb.cdc2f798) *Bulletin of the AAS*, 53(4).  
- [*Walking the Clouds; An Anthology of Indigenous Science Fiction*](https://uapress.arizona.edu/book/walking-the-clouds) 

---

[Lucile Olympe Haute](https://lucilehaute.fr/) is an artist, lecturer in design at Université de Nîmes (FR) and associate researcher at École des arts décoratifs of Paris (FR). 

David Benqué is a designer and researcher based in Cork (IE). He holds a PhD from the Royal College of Art and now operates independently as the [Institute of Diagram Studies](https://diagram.institute/).

Acknowledgements: We would like to thank the anonymous participants in our inaugural session, and [Aaron MacSween](https://cryptography.dog/) for sharing his expert knowledge on peer-to-peer protocols and proof-reading

Please see our [research log](https://www.are.na/institute-of-diagram-studies/sigil-seance-against-space-billionaires) on Are.na

---

## References

