#!/bin/bash

pandoc paper.md \
-f markdown+smart \
-t html -s \
-o index.html \
--toc \
-c /styles.css?20240607123 \
-c paper-styles.css?20240607123 \
--citeproc